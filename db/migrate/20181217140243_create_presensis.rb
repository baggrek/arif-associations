class CreatePresensis < ActiveRecord::Migration[5.2]
  def change
    create_table :presensis do |t|
      t.references :user, foreign_key: true
      t.references :jadwal, foreign_key: true

      t.timestamps
    end
  end
end
