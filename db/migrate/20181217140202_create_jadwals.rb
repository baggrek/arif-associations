class CreateJadwals < ActiveRecord::Migration[5.2]
  def change
    create_table :jadwals do |t|
      t.string :name

      t.timestamps
    end
  end
end
