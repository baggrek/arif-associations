class Jadwal < ApplicationRecord
  has_many :presensis
  has_many :users, :through => :presensis
end
