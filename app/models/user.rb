class User < ApplicationRecord
  has_many :presensis
  has_many :jadwals, :through => :presensis
end
